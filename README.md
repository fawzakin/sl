# Suckless Build
![suckless](dwm-dracula.png)

This repository contains a brand-new build of suckless tools plus nsxiv. They are meant for distros that still use xorg like Debian or Linux Mint.

Version numbering (December 2024):
- dwm: `6.5`
- st: `0.9.2`
- slock: `1.5`
- nsxiv: `32`

# Install
Simply run `install.sh`. It will install all dependencies including rofi and stow for the config. Required fonts (JetBrainsMono Nerd Font and Iosevka SS14) are included, albeit in an extremely limited capacity.

# Colors
![adwaita](dwm-adwaita.png)
You can change the color to adwaita-suitable (blue) color by setting the `#define USE_DRACULA` macro to 0 on every `config.def.c`. You can run `color.sh` to automatically toggle color theme on every program plus rofi depending on what `USE_DRACULA` is set on `dwm/config.def.h`.

# New "Features"
### General
- Less patches applied because less is more.
- All required files (startup, config, etc) are built-in in this repo.
    - `stow` is required to install them.
    - My dotfiles repo is no longer required for a functional dwm session (but still recommended for few rofi scripts).
- There should be no warning messages during compilation that offend one of my lecturer at college.
- Everything looks (almost) the same as the old suckless build and the Hyprland setup.
- dmenu is dropped in favor of rofi. You won't find any dmenu build in this repo.
- tabbed is dropped forever due to lack of use.
- sxiv is "upgraded" to nsxiv which comes with stability and bug fixes.

### dwm
- No bullshit gaps that wastes empty spaces for so-called "aesthetic".
    - No options to change the gap size or toggle the gap during runtime. Use what you have!
    - The bar glued to the bottom screen.
    - No gap when only one window in tag.
    - The only exception is for the small gaps when there are more than one window in a tag to easily identify active window (in other words, actual practicality).
- You can't quit by using keybinding to prevent accidental quit.
- `dwmstatusbar` is now part of `conf`.
    - It should refresh everything, including the time, for every 5 seconds.
    - The icons are adjusted and colors are applied depending on the situation of each component (e.g. CPU turns red when there's more than 80% load).
    - You can now optionally show music title using playerctl if `DWMSTATUSBAR_SHOW_MUSIC` environment variable is set to 1
    - Contains different display mode that can be set using `DWMSTATUSBAR_DISPLAY` environment variable:
        - `laptop`: The default value if not set.
        - `desktop`: Hides battery display.
        - `server`: Hides battery, volumes, and music display but shows internal storage.
- Each component that used to make up for `dwmcom` are now its own scripts in `conf`. The following are the division:
    - `logout_menu()`: Built-in ("exitmenu" patch)
    - `reload_bar()`: `rlbar`
    - `kill_bar()`: `rlbar -k`
    - `xprop_dialog()`: `xpropyad`
    - `copy_color()`: `maimshot -r`
    - `locker()`: `autolocker`
- The monitor can be changed using `xmonitor` just like in Hyprland.
- The screenshot utility is replaced with `maimshot` which requires `maim`. No more fiddling around with Flameshot's inability to set `HOME` directory automatically.
- List of removed patch from the old 6.2 build:
    - alternativetags: Barely used. The new build no longer uses icon as its tag.
    - autostart: Can be done through `.xinitrc`.
    - swallow: I have become used to not swallow windows since configuring it in Hyprland is a nightmare.
    - focusmaster: Just use focusdir lol.
    - focusurgent: Barely used.
    - wrap: Unpatched behavior is now preferred.
    - xresources/xrdb: I'm settled with dracula theme.

### st
- Launches quickly even before your finger leaves the return/enter key!
- All externalpipe scripts that required dmenu now use embedded rofi window.
- List of removed patch from the old 0.8.4 build:
    - alpha: Barely used. I hate transparency.
    - boxdraw: Box drawing already looks good enough without it.
    - vertcenter: No issue found.
    - xresources/xrdb: I'm settled with dracula theme.

### slock
- Blurs the background for "MAXIMUM PRIVACY". /s
- Comes with artistically-inclined lock icon.
- Mediakey won't be block, I hope.
- Seriously, what else should you ask for a locker?

### nsxiv
- Configured to use Dracula theme and sensible font without xresources.
- Better mouse keybinding.
- Intergrated `nsxiv-rifle` inside desktop file to make it act like a normal image viewer through file manager.

# Patch list: dwm
| Patch Name | Description |
| --- | --- |
| [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/) | Toggle real fullscreen for a window |
| [alwayscenter](https://dwm.suckless.org/patches/alwayscenter/) | Put every new floating window at center | 
| [attachbottom](https://dwm.suckless.org/patches/attachbottom/) | New window is placed at the bottom of the stack |
| [borderbar](https://github.com/siduck/chadwm/commit/5f8df23a1f66894a3917c1ed68ffe8838e0fe74c#diff-82241d5b895d1ab27a6646b6953d0e1a1456a6096628f71ecfa155e88bc762f9R1255) | Add border on the status bar (practically, it adds internal padding) |
| [cfacts](https://dwm.suckless.org/patches/cfacts/) | Adjust a window's height in the stack |
| [cyclelayouts](https://dwm.suckless.org/patches/cyclelayouts/) | Click on the layout icon to cycle the layout |
| [exitmenu](https://dwm.suckless.org/patches/exitmenu/) | Build the exit menu directly inside dwm | 
| [fixborder](https://dwm.suckless.org/patches/alpha/) | Fix unwanted transparent border when using picom |
| [focusdir](https://github.com/bakkeby/patches/blob/master/dwm/dwm-focusdir-6.5.diff) | Use directional button to focus on other window |
| [focusonclick](https://dwm.suckless.org/patches/focusonclick/) | Click on a window to focus instead of just hovering it with the cursor |
| [nmaxmaster](https://dwm.suckless.org/patches/nmaxmaster/) | Limit the amount of windows in the master area to 2 |
| [noborder](https://dwm.suckless.org/patches/noborder/) | Remove border on a window if it's the only one in a tag | 
| [pertag](https://dwm.suckless.org/patches/pertag/) | Make each tag act separately (mfact, cfact, etc.) |
| [placedir](https://github.com/bakkeby/patches/blob/master/dwm/dwm-placedir-6.5.diff) | Use directional button to move a focused window around |
| [preserveonrestart](https://dwm.suckless.org/patches/preserveonrestart/) | Do not put all windows in the first tag on a restart |
| [restartsig](https://dwm.suckless.org/patches/restartsig/) | Restart dwm from a keybinding |
| [scratchpads](https://dwm.suckless.org/patches/scratchpads/) | Add toggleable floating windows |
| [status2d](https://dwm.suckless.org/patches/status2d/) | Add color background to statusbar |
| [systray](https://dwm.suckless.org/patches/systray/) | You guess it! | 
| [systrayiconsize](https://gitlab.com/-/snippets/2184056) | Adjust systray icon size |
| [underlinetags](https://dwm.suckless.org/patches/underlinetags/) | Add extra line on active tags |
| [vanitygaps](https://dwm.suckless.org/patches/vanitygaps/) | Adds gaps ONLY when there's more than one window in a tag |

# Patch List: st
| Patch Name | Description |
| --- | --- |
| [anysize](https://st.suckless.org/patches/anysize/) | Allow st to resize to any window geometry |
| [bold is not bright](https://st.suckless.org/patches/bold-is-not-bright/) | self-explanatory | 
| [clipboard](https://st.suckless.org/patches/clipboard/) | Fix clipboard issue |
| [externalpipe](https://st.suckless.org/patches/externalpipe/) | Read the terminal's output for rofi script |
| [font2](https://st.suckless.org/patches/font2/) | Extra fallback font |
| [ligature](https://st.suckless.org/patches/ligatures/) | Join certain combination of symbols together |
| [scrollback](https://st.suckless.org/patches/scrollback/) | Literally the basic of any terminal | 
| [workingdir](https://st.suckless.org/patches/workingdir/) | Specify the starting working directory on launch| 

# Patch List: slock
| Patch Name | Description |
| --- | --- |
| [dpms](https://tools.suckless.org/slock/patches/dpms/) | Turn off screen after some times |
| [foreground and background](https://tools.suckless.org/slock/patches/foreground-and-background/) | Add pixel image and blur background |
| [mediakeys](https://tools.suckless.org/slock/patches/mediakeys/) | Passthrough media key (e.g. volume button) |
| [user](https://tools.suckless.org/slock/patches/user/) | Use the `$USER` variable to determine user and group |

# Patch List: nsxiv
| Patch Name | Description |
| --- | --- |
| [color-invert](https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/patches/color-invert) | Invert color with the `i` key |
| [image-mode-cycle](https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/patches/image-mode-cycle) | Cycle through image list when viewed using `nsxiv-rifle` |
| [zoom-per-image](https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/patches/zoom-per-image) | Each images maintain its own zoom level |
