#!/usr/bin/env bash
rmci() { rm -f config.h && sudo make clean install && rm -f config.h && make clean; }

# Determine if dwm (and only dwm) uses Dracula theme
if grep -q "#define USE_DRACULA 1" dwm/config.def.h; then
    echo "Switching to Adwaita!"
    sed -i "s/^#define[[:space:]]\+USE_DRACULA[[:space:]]\+[0-9]\+/#define USE_DRACULA 0/" dwm/config.def.h
    sed -i "s/^#define[[:space:]]\+USE_DRACULA[[:space:]]\+[0-9]\+/#define USE_DRACULA 0/" st/config.def.h
    sed -i "s/^#define[[:space:]]\+USE_DRACULA[[:space:]]\+[0-9]\+/#define USE_DRACULA 0/" nsxiv/config.def.h
    sed -i 's/^@theme.*/@theme "~\/.config\/rofi\/theme-notdracula.rasi"/' conf-rofi/.config/rofi/config.rasi
else
    echo "Switching to Dracula!"
    sed -i "s/^#define[[:space:]]\+USE_DRACULA[[:space:]]\+[0-9]\+/#define USE_DRACULA 1/" dwm/config.def.h
    sed -i "s/^#define[[:space:]]\+USE_DRACULA[[:space:]]\+[0-9]\+/#define USE_DRACULA 1/" st/config.def.h
    sed -i "s/^#define[[:space:]]\+USE_DRACULA[[:space:]]\+[0-9]\+/#define USE_DRACULA 1/" nsxiv/config.def.h
    sed -i 's/^@theme.*/@theme "~\/.config\/rofi\/theme.rasi"/' conf-rofi/.config/rofi/config.rasi
fi

# Recompile affected programs
sleep 1
cd dwm && rmci
cd ../st && rmci
cd ../nsxiv && rmci
