#!/usr/bin/env bash

source $HOME/.config/rofi-script/conf
source $HOME/.config/rofi-script/copy
source $HOME/.config/rofi-script/radio
source $HOME/.config/rofi-script/wall
source $HOME/.config/rofi-script/web

script_conf() {
  # Only show the list if the file itself exists
  declare -A file
  for i in "${!conf[@]}"; do
    [[ -f ${conf["${i}"]} ]] && file["${i}"]=${conf["${i}"]}
  done

  # Open the config in $EDITOR
  choice=$(printf '%s\n' "${!file[@]}" | sort -f | ${ROFI_CONF} 'Edit Config:' "$@")
  if [ "${choice}" ]; then
    ${TERMINAL:=st} -e "${EDITOR:=nvim}" "$(printf '%s\n' "${file["${choice}"]}")"
  else
    exit 1
  fi
}

script_copy() {
  script="$(printf '%s\n' "${!copy[@]}" | sort -f | ${ROFI_COPY} "Copy:" "$@")"
  [ -z "${script}" ] && exit 1
  pick=${copy["${script}"]}
  if [ "$XDG_SESSION_TYPE" = "x11" ]; then
    echo "${pick}" | xclip -selection clipboard && notify-send -u normal "Copied \"${script}\"" "${pick}"
  else
    echo "${pick}" | wl-copy && notify-send -u normal "Copied \"${script}\"" "${pick}"
  fi
}

script_web() {
  engine=$(printf '%s\n' "${!web[@]}" | sort -f | ${ROFI_WEB} "Search (${BROWSER}):" "$@") || exit 1
  url="${web["${engine}"]}"
  query=$(echo "" | ${ROFI_QUERY} "Query (${engine})")
  ${BROWSER:=firefox} "${url}${query}"
}

script_radio() {
  if [ "$USE_PLAYLIST" -eq 1 ]; then
    [ ! -f "$PLAYLIST" ] && notify-send "PLAYLIST is not set or found" && exit 1
    station="$(cat "${PLAYLIST}" | grep "#EXTINF" | sed -E 's/^#EXTINF:(.*),//' | ${ROFI} "Radio:" "$@")" || exit 1
    url="$(cat "${PLAYLIST}" | grep -A1 "$station" | sed -n 2p)"
  else
    station=$(printf '%s\n' "${!radio[@]}" | sort -f | ${ROFI_RADIO} "Radio: " "$@") || exit 1
    url="${radio["${station}"]}"
  fi

  notify-send "Playing Radio" "$station"

  DRISPID="$(ps aux | grep mpDris2 | grep -v grep | awk '{print $2}')"
  [ -n "$DRISPID" ] && kill $DRISPID
  mpDris2 &
  mpc clear
  mpc add "$url"
  mpc play

  ## Only for dwmstatusbar
  [ -x "$(command -v rlbar)" ] && rlbar
}

script_wall() {
  [ ! -d "$WALLPAPER_DIR" ] && notify-send "${WALLPAPER_DIR:=WALLPAPER_DIR} not found" && exit 1
  
  IMG="$(find $WALLPAPER_DIR -maxdepth 1 \( -name "*.png" -o -iname "*.jpg" \) -printf "%f\n" | sort -f | ${ROFI_WALL} "Wallpaper: " "$@")" || exit 1
  APPLY="${WALLPAPER_DIR}/${IMG}"

  # Option 1: Using hyprpaper
  mkdir -p "${WALLPAPER_DIR}/apply"
  ln -sf "$APPLY" "${WALLPAPER_DIR}/apply/main.png"

  if [ "$XDG_SESSION_TYPE" = "x11" ]; then
    apply_xorg "$APPLY"
  elif [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    apply_wayland "$APPLY" 
  fi

  notify-send "Changed Wallpaper!" "$IMG"
}

help() {
  echo "USAGE:"
  echo "-c/--conf   Edit configurations."
  echo "-p/--copy   Copy pre-defined value to clipboard."
  echo "-w/--web    Search the web from listed sites."
  echo "-l/--wall   Set wallpaper."
  echo "-r/--radio  Play radio from a list or a playlist."
  echo ""
  echo "Dependencies:"
  echo "    All: rofi mpd mpc"
  echo "    X11: xclip xwallpaper"
  echo "Wayland: wl-copy swww"
  exit 0
}

case "$1" in
-c | --conf | --config) script_conf ;;
-p | --copy)  script_copy ;;
-w | --web)   script_web  ;;
-l | --wall)  script_wall ;;
-r | --radio) script_radio;;
*) help ;;
esac
