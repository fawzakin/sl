#!/usr/bin/env sh

issaved=0
action="Something Wrong Happened!"

dir="$HOME/Pictures/Screenshots"
nam="$(date "+%Y-%m-%d_%H-%M-%S-%3N")"
sav="${dir}/${nam}.png"
lst="$HOME/.cache/last_screenshot"
out="${nam}.png"

mkdir -p ${dir}

# Capture everything that's shown on the monitor.
get_all() {
  maim "$sav" && \
    issaved=1 && action="Captured Everything!" || exit 1
}

# Capture the currently focused monitor.
# NOTE: This is currently hardcoded to only support up to two 1080p screens!
get_mon() {
  pos="$(xdotool getactivewindow getwindowgeometry | awk '/Position:/ {print $2}' | cut -d"," -f1)" || notify-send "error?"
  [ -z $pos ] && pos=0
  if [ $pos -lt 1920 ]; then
    maim "$sav" --geometry=1920x1080+0+0 && \
      issaved=1 && action="Captured Monitor 1!" || exit 1
  else
    maim "$sav" --geometry=1920x1080+1920+0 && \
      issaved=1 && action="Captured Monitor 2!" || exit 1
  fi
}

# Capture the currently focused window.
get_win() {
  maim "$sav" --hidecursor --window $(xdotool getactivewindow) && \
    issaved=1 && action="Captured Current Window!" || exit 1
}

# Select which window/area you want to capture.
get_sel() {
  maim "$sav" --hidecursor --select -b 2 -c 1.0,1.0,1.0,0.4 -l && \
    issaved=1 && action="Captured Selected Box!" || exit 1
}

# This doesn't save anything, but rather, it gets RGB value of selected pixel
# (or average RGB when selected an area)
get_rgb() {
  rgb="$(maim -ust 0 | magick - -resize 1x1\! -format '%[pixel:p{0,0}]' info:-)" && \
    issaved=0 && action="Obtained RGB value!" || exit 1
  if echo "$rgb" | grep -q "%"; then
    nam="$rgb"
  else
    nam=$(get_hex "${rgb}")
  fi
  echo "$nam" | xclip -selection clipboard
  sav="$nam"
}

# Convert imagemagick's SRGBA into hex color
get_hex() {
  rgba=$(echo "$1" | sed -E 's/srgba\(([^,]+),([^,]+),([^,]+),([^)]+)\)/\1 \2 \3 \4/')
  hex=$(echo "$rgba" | awk '{
    r = ($1 ~ /%$/) ? int(substr($1, 1, length($1)-1) * 255 / 100) : $1;
    g = ($2 ~ /%$/) ? int(substr($2, 1, length($2)-1) * 255 / 100) : $2;
    b = ($3 ~ /%$/) ? int(substr($3, 1, length($3)-1) * 255 / 100) : $3;
    a = ($4 ~ /%$/) ? substr($4, 1, length($4)-1) : $4 * 255;
    if (a == 255) {
      printf("#%02X%02X%02X\n", r, g, b);
    } else {
      printf("#%02X%02X%02X%02X\n", r, g, b, a);
    }
  }')
  echo "$hex"
}

# Copy saved content to clipboard and cache the last saved image
get_clp() {
  cat "$sav" | xclip -selection clipboard -target image/png || exit 1
  echo "$sav" > "$lst"
}

# Open image
open() {
  ${IMAGE:=nsxiv} "$(cat $lst)" || \
    { notify-send "Cannot open image!" "$(cat $lst)"; exit 1; }
  exit 0
}

# Show help message
help() {
  echo "USAGE:"
  echo "-a/--all      Capture everything that's visible to all monitor."
  echo "-m/--monitor  Capture currently focused monitor."
  echo "-w/--window   Capture currently focused window."
  echo "-s/--select   Select window or area to capture."
  echo "-r/--rgb      Get RGB value (requires imagemagick)"
  echo "-o/--open     Open the last saved image."
  echo ""
  echo "Dependencies: maim xclip imagemagick"
  exit 0
}

# Get key 
case "$1" in
-a | --all)     get_all ;;
-m | --monitor) get_mon ;;
-w | --window)  get_win ;;
-s | --select)  get_sel ;;
-r | --rgb)     get_rgb ;;
-o | --open)    open;;
*) help ;;
esac

# Copy to clipboard (always) and cache the location for the last-captured screenshot.
# Only when issaved is set to 1
[ $issaved -eq 1 ] && get_clp

# Send notification by the end
notify-send "$action" "$sav"
