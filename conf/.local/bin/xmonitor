#!/usr/bin/env sh

PICK="$HOME/.cache/xmonitor_pick"
touch $PICK

hdmi_audio() {
  audio="$(pactl list sinks | grep "Name:" | grep "HiFi__HDMI2" | awk '{print $2}')"
  pactl set-default-sink "${audio}"
}

laptop_audio() {
  audio="$(pactl list sinks | grep "Name:" | grep "HiFi__Speaker" | awk '{print $2}')"
  pactl set-default-sink "${audio}"
}

wall() {
  xwallpaper --zoom "${WALLPAPER:=$HOME/.local/backgrounds/dracula.png}"
}

reset() {
  xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal \
         --output HDMI-1 --off
  laptop_audio
  wall
}

dual() {
  xrandr --output eDP-1 --mode 1920x1080 --pos 1920x0 --rotate normal \
         --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal
  hdmi_audio
  wall

  echo "dual" > $PICK
  notify-send "Switched to dual monitor!"
}

hdmi() {
  xrandr --output eDP-1 --off \
         --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal
  hdmi_audio
  wall

  echo "hdmi" > $PICK
  notify-send "Switched to hdmi monitor!"
}

laptop() {
  reset

  echo "laptop" > $PICK
  notify-send "Switched to laptop monitor!"
}

mirror() {
  xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal \
         --output HDMI-1 --mode 1920x1080 --pos 0x0 --rotate normal
  laptop_audio
  wall

  echo "mirror" > $PICK
  notify-send "Monitor Mirrored!"
}

autopick() {
  NEWPICK="$(cat $PICK)"
  echo "Picking $NEWPICK"
  case "$NEWPICK" in
    "dual") dual; exit 0;;
    "hdmi") hdmi; exit 0;;
    "laptop") laptop; exit 0;;
    "mirror") mirror; exit 0;;
    *) echo "Nothing Todo"; exit 0;;
  esac
}

help() {
  echo "USAGE:"
  echo "[NO OPTION]   Select monitor (only when external monitor is connected)."
  echo "-r | --reset  Force reset the monitor to only use laptop monitor."
  echo "-a | --auto   Select last picked monitor layout. Only use this on startup."
  echo "-h | --help   Show this message."
  exit 0
}

# These are options triggered regardless if the monitor is connected or not.
case "$1" in
  --help|-h) help ;;
  --reset|-r ) reset; exit 0 ;;
esac

# These are triggered if you are not connected to a monitor
if xrandr | grep -q "HDMI-1 disconnected"; then
  case "$1" in
    --auto|-a) reset; exit 0 ;;
    *) notify-send -u normal "No Monitor Detected"; exit 1 ;;
  esac
fi

# The rest from here are triggered if the script passes the no-monitor check.
# In other word, actually do stuff when an external monitor is connected.
case "$1" in
  --auto|-a) autopick; exit 0;;
esac

# In order to pick monitor setting, don't put any options!
monopt="$(printf \
'󰍹 󰌢  Dual Monitor
󰍹 󰛧  HDMI Monitor
󰶐 󰌢  Laptop Monitor
󰐮 󰌢  Mirror Monitor
󰌢 󰓃  Speaker eDP-1
󰍹 󰓃  Speaker HDMI' \
| rofi -dmenu -l 6 -p "Monitor:")"

case ${monopt} in
    *Dual\ Monitor*)    dual ;;
    *HDMI\ Monitor*)    hdmi ;;
    *Laptop\ Monitor*)  laptop ;;
    *Mirror\ Monitor*)  mirror ;;
    *Speaker\ eDP-1*)   laptop_audio ;;
    *Speaker\ HDMI*)    hdmi_audio ;;
    *) exit 1 ;;
esac
