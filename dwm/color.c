#if USE_DRACULA // Dracula colorscheme
static const char col_fg[]	= "#D3D7CF"; // Foreground
static const char col_bg[]	= "#282A36"; // Background
static const char col_dg[]	= "#1E1F28"; // Darker Background
static const char col_ac[]	= "#725E97"; // Accent Color
static const char col_lc[]	= "#BD93F9"; // Ligher Accent Color
static const char col_bo[]	= "#323641"; // Border
#else // Adwaita Dark
static const char col_fg[]	= "#D7D7D7"; // Foreground
static const char col_bg[]	= "#282828"; // Background
static const char col_dg[]	= "#222222"; // Darker Background
static const char col_ac[]	= "#3465a4"; // Accent Color
static const char col_lc[]	= "#729fcf"; // Ligher Accent Color
static const char col_bo[]	= "#343434"; // Border
#endif
