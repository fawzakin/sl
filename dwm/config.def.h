/* Appearance */
static const unsigned int borderpx	= 3;	/* border pixel of windows */
static const unsigned int barpadpx	= 3;	/* border pixel of statusbar */
static const unsigned int snap		= 8;	/* snap pixel */
static const unsigned int gappih	= 4;	/* horiz inner gap between windows */
static const unsigned int gappiv	= 4;	/* vert inner gap between windows */
static const unsigned int gappoh	= 4;	/* horiz outer gap between windows and screen edge */
static const unsigned int gappov	= 4;	/* vert outer gap between windows and screen edge */
static const int smartgaps		= 1;	/* 1 means no outer gap when there is only one window */
static const int showbar		= 1;	/* 0 means no bar */
static const int topbar			= 0;	/* 0 means bottom bar */

/* Underline Bar */
static const unsigned int ulinepad	= 0;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 3;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 30;	/* how far above the bottom of the bar the line should appear */
static const int ulineall		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

/* Behaviors */
static const int nmaxmaster	= 2;		/* maximum number of clients allowed in master area */
static const int focusonwheel   = 0;		/* Use mousewheel to focus */

/* Systray */ 
static const int showsystray		  = 1;	/* 0 means no systray */
static const int systraypinningfailfirst  = 1;	/* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const unsigned int systraypinning  = 0;	/* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft   = 0;	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing  = 0;	/* systray spacing */
static const unsigned int systrayiconsize = 28; /* systray icon size in px */

/* Fonts */
static const char font[]	= "Iosevka SS14 Medium:size=14:antialias=true:autohint=true";   /* Main font */
static const char fonticon[]	= "JetBrainsMono Nerd Font:style=Medium:pixelsize=16:antialias=true:autohint=true";
static const char *fonts[]	= { font, fonticon };

/* Color Scheme */
#define USE_DRACULA 1 
#include "color.c"
static const char *xcolor[]	= { col_fg, col_bg, col_dg, col_ac, col_bo, col_lc };
static const char *colors[][3]  = {
	/*               fg      bg      border   */
	[SchemeNorm] = { col_fg, col_bg, col_bo},
	[SchemeSel]  = { col_ac, col_bg, col_lc},
};

/* Tagging */
static const char *tags[] = { "𝟭", "𝟮", "𝟯", "𝗤", "𝗪", "𝗘", "𝗔", "𝗦", "𝗗" };

/* Scratchpads */
typedef struct { const char *name; const void *cmd; } Sp;
const char *spcmd0[] = {"st", "-n", "st-float", "-g", "120x30", NULL };
const char *spcmd1[] = {"st", "-n", "st-music", "-g", "120x30", "-e", "ncmpcpp", NULL };
const char *spcmd2[] = {"st", "-n", "st-top", "-g", "120x30", "-e", "btop", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"st-float",	spcmd0},
	{"st-music",	spcmd1},
	{"st-top",	spcmd2},
};

/* Rules */
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class		instance	title		tags mask	float	monitor */
	{ "firefox",		NULL,		NULL,		1 << 2,		0,	-1 },
	{ "LibreWolf",		NULL,		NULL,		1 << 2,		0,	-1 },      
	{ "Brave-browser",	NULL,		NULL,		1 << 2,		0,	-1 }, 
        { "Code",		NULL,		NULL,		1 << 3,		0,	-1 },
	{ "VSCodium",		NULL,		NULL,           1 << 3,		0,	-1 },
	{ "neovide",		NULL,		NULL,           1 << 3,		0,	-1 },
        { "DesktopEditors",	NULL,		NULL,		1 << 4,		0,	-1 },
        { "Godot",		NULL,		NULL,		1 << 4,		0,	-1 },
        { "Gimp",		NULL,		NULL,           1 << 5,		0,	-1 },
	{ "Pinta",		NULL,		NULL,           1 << 5,		0,	-1 },
	{ "krita",		NULL,		NULL,           1 << 5,		0,	-1 },
	{ "Inkscape",		NULL,		NULL,		1 << 5,		0,	-1 },
	{ "Aseprite",		NULL,		NULL,		1 << 5,		0,	-1 },
	{ "Chromium",		NULL,		NULL,		1 << 6,		0,	-1 }, 
	{ "zoom",		NULL,		NULL,           1 << 6,		1,	-1 },
	{ "discord",		NULL,		NULL,           1 << 6,		0,	-1 },
	{ "Steam",		NULL,		NULL,           1 << 8,		0,	-1 },
	{ "Dragon-drop",	NULL,		NULL,           0,		1,	-1 },
	{ "Yad",	        NULL,	        NULL,           0,	        1,      -1 },
 	{ NULL,			"st-float",	NULL,		SPTAG(0),	1,	-1 },
 	{ NULL,			"st-music",	NULL,		SPTAG(1),	1,	-1 },
 	{ NULL,			"st-top",	NULL,		SPTAG(2),	1,	-1 },
};

/* Layouts */
static const float mfact	= 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster	= 1;	/* number of clients in master area */
static const int resizehints	= 1;	/* 1 means respect size hints in tiled resizals */
static const int lockfullscreen	= 1;	/* 1 will force focus on the fullscreen window */

#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* Key Definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* Commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "rofi", "-show", "combi", "-modes", "combi", "-combi-modes", "drun,run", "-show-icons", NULL }; 
static const char *termcmd[]  = { "st", NULL };
static const char *browser[]  = { "dwm-browser", NULL }; // Default to Firefox
static const char *filemgr[]  = { "dwm-file", NULL };	 // Default to pcmanfm
static const char *xkill[]    = { "xkill", NULL };

/* Keybinding */
#include <X11/XF86keysym.h>
#include "exitdwm.c"
static const Key keys[] = {
	/* STARTKEY: Do not delete this comment line */
   	// =======================
   	// ## Window Management ##
   	// =======================
	{ MODKEY|ShiftMask,		XK_Escape,	quit,		{1} }, 
	{ MODKEY,			XK_grave,	killclient,	{0} },
	{ MODKEY,			XK_Tab,		view,		{0} },
	{ ALTKEY,			XK_Tab,		view,		{0} },
	{ MODKEY,			XK_h,		focusdir,	{.i = 0 } },
	{ MODKEY,			XK_l,		focusdir,	{.i = 1 } },
	{ MODKEY,			XK_k,		focusstack,     {.i = -1 } },
	{ MODKEY,			XK_j,		focusstack,	{.i = +1 } },
	{ MODKEY|ShiftMask,		XK_h,		placedir,	{.i = 0 } }, 
	{ MODKEY|ShiftMask,		XK_l,		placedir,	{.i = 1 } },
	{ MODKEY|ShiftMask,		XK_k,		placedir,	{.i = 2 } },
	{ MODKEY|ShiftMask,		XK_j,		placedir,	{.i = 3 } },
	{ MODKEY|ControlMask,		XK_h,		setmfact,	{.f = -0.05} },
	{ MODKEY|ControlMask,		XK_l,		setmfact,	{.f = +0.05} },
	{ MODKEY,			XK_i,		setcfact,	{.f = +0.20} },
	{ MODKEY,			XK_u,		setcfact,	{.f = -0.20} },
	{ MODKEY,			XK_o,		setcfact,	{.f =  0.00} },
	{ MODKEY,			XK_bracketleft, incnmaster,	{.i = -1 } },
	{ MODKEY,			XK_bracketright,incnmaster,	{.i = +1 } },
	{ MODKEY,			XK_semicolon,	cyclelayout,	{.i = -1 } },
	{ MODKEY,			XK_apostrophe,	cyclelayout,	{.i = +1 } },

   	// ====================
   	// ## Toggle Buttons ##
   	// ====================
	{ MODKEY,			XK_t,		togglebar,	{0} },
	{ MODKEY,			XK_f,		togglefloating,	{0} },
	{ MODKEY,			XK_g,		togglefullscr,	{0} },

   	// =====================
   	// ## Monitor Control ##
   	// =====================
	{ MODKEY,			XK_comma,	focusmon,	{.i = -1 } },
	{ MODKEY,			XK_period,	focusmon,	{.i = +1 } },
	{ MODKEY|ShiftMask,		XK_comma,	tagmon,		{.i = -1 } },
	{ MODKEY|ShiftMask,		XK_period,	tagmon,		{.i = +1 } },

   	// =======================
   	// ## Program Launching ##
   	// =======================
	{ MODKEY,			XK_slash,	spawn,		{.v = dmenucmd } },
	{ MODKEY,			XK_Return,	spawn,		{.v = termcmd } },
 	{ MODKEY,			XK_b,		spawn,		{.v = browser } },
 	{ MODKEY,			XK_m,		spawn,		{.v = filemgr } },
 	{ MODKEY,			XK_Delete,      spawn,		{.v = xkill } },
 	{ MODKEY,			XK_backslash,	togglescratch,	{.ui = 0 } },
 	{ MODKEY,			XK_n,		togglescratch,	{.ui = 1 } },
 	{ MODKEY,			XK_v,		togglescratch,	{.ui = 2 } },

   	// ==================
   	// ## Rofi Scripts ##
   	// ==================
 	{ MODKEY,			XK_Escape,	exitdwm,	{0} },
        { 0,			  XF86XK_PowerOff,      exitdwm,        {0} },
	{ MODKEY,		         XK_Home,       spawn,          SHCMD("rofimoji --action type copy") },
	{ MODKEY,		     XK_BackSpace,      spawn,      	SHCMD("rofi-script --web") },
	{ MODKEY,	                 XK_equal,      spawn,          SHCMD("rofi-script --copy") },
	{ MODKEY,		         XK_minus,	spawn,          SHCMD("rofi-script --conf") },
	{ MODKEY,		         XK_0,	        spawn,          SHCMD("xmonitor") },
	{ MODKEY|ShiftMask,		 XK_0,	        spawn,          SHCMD("xmonitor --reset") },
	{ MODKEY,		         XK_9,	        spawn,          SHCMD("rofi-script --wall") },
	{ MODKEY,		         XK_8,	        spawn,          SHCMD("rofi-script --radio") },
	{ MODKEY|ShiftMask,	         XK_8,	        spawn,          SHCMD("playerctl-like || mpc-like") },
	{ MODKEY|ShiftMask,	         XK_m,	        spawn,          SHCMD("mount-webdav") },

   	// ================
   	// ## Screenshot ##
   	// ================
	{ 0,				XK_Print,	spawn,		SHCMD("maimshot -m") },	// Mon
	{ ControlMask,			XK_Print,	spawn,		SHCMD("maimshot -s") },	// Select
	{ ShiftMask,			XK_Print,	spawn,		SHCMD("maimshot -w") }, // Window
	{ ControlMask|ShiftMask,	XK_Print,	spawn,		SHCMD("maimshot -a") },	// All
	{ MODKEY,			XK_Print,	spawn,		SHCMD("maimshot -o") }, // Open
	{ MODKEY,			XK_Insert,	spawn,		SHCMD("maimshot -r") }, // RGB
	{ MODKEY,			XK_Pause,	spawn,		SHCMD("maimshot -r") }, // Dups
	{ MODKEY|ShiftMask,	        XK_Insert,	spawn,		SHCMD("xpropyad") },
	{ MODKEY|ShiftMask,	        XK_Pause,	spawn,		SHCMD("xpropyad") },

   	// ====================
   	// ## Multimedia key ##
   	// ====================
	{ 0,                 XF86XK_AudioRaiseVolume,   spawn,          SHCMD("pamixer -i 5 && rlbar") },
	{ 0,                 XF86XK_AudioLowerVolume,	spawn,          SHCMD("pamixer -d 5 && rlbar") },
	{ 0,                        XF86XK_AudioMute,   spawn,          SHCMD("pamixer -t && rlbar") },
	{ 0,		      XF86XK_MonBrightnessUp,	spawn,		SHCMD("xbacklight -inc 5") },
	{ 0,		    XF86XK_MonBrightnessDown,	spawn,		SHCMD("xbacklight -dec 5") },
	{ 0,			    XF86XK_AudioPlay,	spawn,		SHCMD("playerctl play-pause || mpc toggle && rlbar") },
	{ MODKEY,				XK_4,	spawn,		SHCMD("playerctl play-pause || mpc toggle && rlbar") },
	{ 0,			    XF86XK_AudioStop,	spawn,          SHCMD("playerctl stop || mpc stop && rlbar") },
	{ MODKEY,				XK_5,	spawn,          SHCMD("playerctl stop || mpc stop && rlbar") },
	{ 0,			    XF86XK_AudioPrev,	spawn,          SHCMD("playerctl previous || mpc prev && rlbar") },
	{ MODKEY,				XK_6,	spawn,          SHCMD("playerctl previous || mpc prev && rlbar") },
	{ 0,			    XF86XK_AudioNext,	spawn,          SHCMD("playerctl next || mpc next && rlbar") },
	{ MODKEY,				XK_7,	spawn,          SHCMD("playerctl next || mpc next && rlbar") },

   	// ================
	// ## Tag Labels ##
   	// ================
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_q,                      3)
	TAGKEYS(                        XK_w,                      4)
	TAGKEYS(                        XK_e,                      5)
	TAGKEYS(                        XK_a,                      6)
	TAGKEYS(                        XK_s,                      7)
	TAGKEYS(                        XK_d,                      8)

	/* ENDKEY: Do not delete this comment line */
};

/* Mouse Button Definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1 } },
	{ ClkLtSymbol,          0,              Button3,        cyclelayout,    {.i = -1 } },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
