#!/usr/bin/env bash
echo "Installing required packages"

# On Arch
if command -v pacman &> /dev/null; then
    sudo pacman -S --needed --noconfirm base-devel imlib2 libexif xclip xdotool xorg-server xorg-xinit xorg-xinput xorg-xkill xorg-xsetroot xwallpaper rofi stow pcmanfm

# On Debian
elif command -v apt &> /dev/null; then
    sudo apt install -y build-essential libexif12 libexif-dev libgtkmm-3.0-dev libharfbuzz-dev libimlib2-dev libnotify-bin libx11-xcb-dev libxcb-res0-dev libxcb-xfixes0-dev libxft-dev libxinerama-dev lm-sensors x11-utils xclip xdotool xinit xinput xserver-xorg xwallpaper rofi stow pcmanfm
fi

for dir in dwm st slock nsxiv ; do
    echo "Compiling $dir"
    cd ${dir}
    sudo make clean install
    make clean
    cd ..
done

stow --no-folding -t ~ conf
stow --no-folding -t ~ conf-rofi

echo "To install the full font set, please download them from the sources below:"
echo "* https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/JetBrainsMono.zip"
echo "* https://github.com/be5invis/Iosevka/releases/download/v32.3.1/PkgTTF-IosevkaSS14-32.3.1.zip"
