static const char *colorname[NUMCOLS] = {
	[INIT] =   "#1a1a1a",   /* after initialization */
	[INPUT] =  "#D7D7D7",   /* during input */
	[FAILED] = "#CC0000",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/* insert grid pattern with scale 1:1, the size can be changed with logosize */
static const int logosize = 24;
/* grid width and height for right center alignment */
static const int logow = 14;
static const int logoh = 17;

static XRectangle rectangles[15] = {
	/* x    y       w       h */
	{ 4,	0,      6,      2 },
	{ 3,    1,      2,      2 },
	{ 9,    1,      2,      2 },
	{ 2,    2,      2,      5 },
	{ 10,   2,      2,      5 },
	{ 1,	7,		12,     1 },
	{ 1,	16,     12,     1 },
	{ 0,	8,		5,      8 },
	{ 9,	8,		5,      8 },
	{ 5,	8,		4,      2 },
	{ 5,	14,		4,      2 },
	{ 5,	10,		1,      1 },
	{ 8,	10,		1,      1 },
	{ 5,	13,		1,      1 },
	{ 8,	13,		1,      1 },
};

/*Enable blur*/
#define BLUR
/*Set blur radius*/
static const int blurRadius=8;

/*Enable Pixelation*/
//#define PIXELATION

/*Set pixelation radius*/
static const int pixelSize=0;

/* time in seconds before the monitor shuts down */
static const int monitortime = 120;
